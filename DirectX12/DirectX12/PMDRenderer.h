#pragma once
#include <Windows.h>
#include <d3d12.h>
#include <dxgi1_6.h>
#include <DirectXMath.h>
#include <d3dcompiler.h>
#include <wrl.h>
#include <string>
#include <DirectXTex.h>
#include <memory>
#include <d3dx12.h>
#include <array>

#pragma comment(lib,"DirectXTex.lib")
#pragma comment(lib,"d3d12.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"d3dcompiler.lib")

using Microsoft::WRL::ComPtr;

class Dx12Wrapper;

class PMDRenderer
{
public:
	PMDRenderer() = default;
	PMDRenderer(Dx12Wrapper* dx12);
	~PMDRenderer();
	const ComPtr<ID3D12PipelineState> GetPipelineState();
	const ComPtr<ID3D12RootSignature> GetRootSignature();
private:
	// パイプライン
	void CreatePipelineState();
	// ルートシグネチャ作成
	void InitRootSignature();

	std::array<CD3DX12_STATIC_SAMPLER_DESC, 2> SettingSampler();

	ComPtr<ID3D12Device> dev_ = nullptr;

	ComPtr<ID3D12PipelineState> pipeline_ = nullptr;		// パイプライン
	ComPtr<ID3D12RootSignature> rootSignature_ = nullptr;	// ルートシグネチャ
	ComPtr<ID3DBlob> signatureBlob_ = nullptr; //ルートシグネチャをつくるための材料
	ComPtr<ID3DBlob> errorBlob_ = nullptr;	   //エラー出た時の対処

	ComPtr<ID3DBlob> vertexShader_ = nullptr;	// 頂点シェーダ
	ComPtr<ID3DBlob> pixelShader_ = nullptr;	// ピクセルシェーダ

	Dx12Wrapper* dx12_;
};

