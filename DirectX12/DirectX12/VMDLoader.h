#pragma once
#include <windows.h>
#include <DirectXMath.h>
#include <string>
#include <vector>
#include <map>

#pragma pack(1)

struct VMDMotion
{
	char boneName[15];			// ボーン名
	DWORD frameNo;				// フレーム番号(読込時は現在のフレーム位置を0とした相対位置)
	DirectX::XMFLOAT3 location; // 位置
	DirectX::XMFLOAT4 quaternion;  // 回転
	BYTE interpolation[64];		 // 補完
};

struct Keyframe
{
	Keyframe(int frame, DirectX::XMFLOAT4 quat, DirectX::XMFLOAT3 p, DirectX::XMFLOAT2 x, DirectX::XMFLOAT2 y) :frameno(frame), quaternion(quat),pos(p),b1(x),b2(y) {}
	int frameno;
	DirectX::XMFLOAT4 quaternion;
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT2 b1,b2;
};

#pragma pack(0)

class VMDLoader
{
public:
	VMDLoader();
	bool LoadFile(const char* filePath);
	const std::map<std::string, std::vector<Keyframe>> GetAnimationData();
private:
	std::vector<VMDMotion> keyframes;

	std::map<std::string, std::vector<Keyframe>> animData_;
};

