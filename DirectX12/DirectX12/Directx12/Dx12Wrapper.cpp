#include <assert.h>
#include <tchar.h>
#include <algorithm>
#include "Dx12Wrapper.h"
#include "../pmdModel/PMDModel.h"
#include "../Application.h"
#include "../PMDLoader.h"
#include "../VMDLoader.h"

constexpr float PI = 3.1415926535;

using namespace DirectX;
using namespace std;

Dx12Wrapper::Dx12Wrapper()
{
}

Dx12Wrapper::Dx12Wrapper(HWND hwnd):hwnd_(hwnd)
{
}

Dx12Wrapper::~Dx12Wrapper()
{
	
}

bool Dx12Wrapper::Initialize()
{
	// デバイス生成用関数
	//HRESULT D3D12CreateDevice(
	//	IUnknown		  * pAdapter,
	//	D3D_FEATURE_LEVEL   MinimumFeatureLevel, // フィーチャレベル
	//	REFIID				riid,
	//	void				** ppDevice
	//);

#ifdef _DEBUG
	ID3D12Debug* debugLayer = nullptr;
	auto res = D3D12GetDebugInterface(IID_PPV_ARGS(&debugLayer));
	assert(SUCCEEDED(res));
	debugLayer->EnableDebugLayer();
	debugLayer->Release();
#elif 
#endif // _DEBUG

	CoInitializeEx(0, COINIT_MULTITHREADED);

	D3D_FEATURE_LEVEL levels[] = {
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0
	};

	D3D_FEATURE_LEVEL level = D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_12_1;
	HRESULT result = S_OK;
	for (auto l : levels)
	{
		result = D3D12CreateDevice(nullptr, l, IID_PPV_ARGS(&dev_));
		if (SUCCEEDED(result))
		{
			level = l;
			break;
		}
	}
	// デバイス生成に失敗した場合はassert
	assert(SUCCEEDED(result));

	//HRESULT CreateDXGIFactory2(
	//	UINT	Flags,
	//	REFIID	riid,
	//	void	** ppFactory
	//);

#ifdef _DEBUG
	result = CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, IID_PPV_ARGS(&dxgi_));
#else
	result = CreateDXGIFactory2(0, IID_PPV_ARGS(&dxgi_));
#endif
	assert(SUCCEEDED(result));

	D3D12_COMMAND_QUEUE_DESC cmdQDesc = {};
	cmdQDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	cmdQDesc.NodeMask = 0;
	cmdQDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	cmdQDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	result = dev_->CreateCommandQueue(&cmdQDesc, IID_PPV_ARGS(&cmdQue_));
	assert(SUCCEEDED(result));

	auto& app = Application::Instance();
	auto wsize = app.GetWindowSize();
	DXGI_SWAP_CHAIN_DESC1 scDesc = {};
	scDesc.Width = wsize.w;
	scDesc.Height = wsize.h;
	scDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scDesc.BufferCount = 2;
	scDesc.BufferUsage = DXGI_USAGE_BACK_BUFFER;
	scDesc.Flags = 0;
	scDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	scDesc.SampleDesc.Count = 1;
	scDesc.SampleDesc.Quality = 0;
	scDesc.Scaling = DXGI_SCALING_STRETCH;
	scDesc.Stereo = false;
	scDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	
	dxgi_->CreateSwapChainForHwnd(cmdQue_.Get(),hwnd_, &scDesc,nullptr,nullptr, (IDXGISwapChain1**)(swapchain_.GetAddressOf()));

	rtvHeap_ = nullptr;
	D3D12_DESCRIPTOR_HEAP_DESC descriptorHeapDesc = {};
	descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	descriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	descriptorHeapDesc.NumDescriptors = 2;
	descriptorHeapDesc.NodeMask = 0;
	result = dev_->CreateDescriptorHeap(&descriptorHeapDesc, IID_PPV_ARGS(rtvHeap_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	DXGI_SWAP_CHAIN_DESC swcDesc = {};

	swapchain_->GetDesc(&swcDesc);

	int renderTargetsNum = swcDesc.BufferCount;

	// レンダーターゲット数ぶん確保
	renderTargets_.resize(renderTargetsNum);

	D3D12_CPU_DESCRIPTOR_HANDLE descriptorHandle = {};
	descriptorHandle = rtvHeap_->GetCPUDescriptorHandleForHeapStart();

	// SRGBレンダーターゲットビュー設定
	D3D12_RENDER_TARGET_VIEW_DESC rtvDesc = {};
	rtvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;

	// デスクリプタ1個当たりのサイズを取得
	int descriptorSize = dev_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	for (int i = 0; i < renderTargetsNum; i++)
	{
		result = swapchain_->GetBuffer(i, IID_PPV_ARGS(&renderTargets_[i]));			// 「キャンパス」を取得
		dev_->CreateRenderTargetView(renderTargets_[i].Get(), &rtvDesc, descriptorHandle);	// キャンパスと職人を紐づける
		descriptorHandle.ptr += descriptorSize;		// 職人とキャンパスのペアぶん次のところまでオフセット
	}

	result = dev_->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&cmdAllocator_));
	assert(SUCCEEDED(result));
	result = dev_->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, cmdAllocator_.Get(), nullptr, IID_PPV_ARGS(&cmdList_));
	assert(SUCCEEDED(result));

	pmdModel = std::make_shared<PMDModel>();
	std::string strModelPath = "model/初音ミク.pmd";
	pmdModel->LoadFile(strModelPath.c_str());
	vertices_ = pmdModel->GetVertexData();
	texResources_.resize(pmdModel->GetMaterial().size());
	toonResources_.resize(pmdModel->GetMaterial().size());

	auto& pmdLoader = PMDLoader::Instance();
	texResources_  = pmdLoader.LoadTexture(pmdModel, strModelPath, dev_);
	toonResources_ = pmdLoader.LoadToon(pmdModel, strModelPath, dev_);

	boneMatrix_.resize(pmdModel->GetBone().size());
	std::fill(boneMatrix_.begin(),boneMatrix_.end(),XMMatrixIdentity());

	auto& mbones = pmdModel->GetBone();
	for (int idx = 0; idx < mbones.size(); ++idx) {
		auto& b = pmdModel->GetBone()[idx];
		auto& boneNode = boneMap_[b.bone_name];
		boneNode.boneIdx = idx; 
		boneNode.startPos = b.bone_head_pos; 
		boneNode.endPos = mbones[b.tail_pos_bone_index].bone_head_pos;
	}

	for (auto& b : boneMap_)
	{
		if (mbones[b.second.boneIdx].parent_bone_index >= mbones.size())
		{
			continue;
		}
		
		auto parentName = mbones[mbones[b.second.boneIdx].parent_bone_index].bone_name;
		boneMap_[parentName].children.push_back(&b.second);
	}

	vmdLoader_ = std::make_unique<VMDLoader>();
	std::string strMotionPath = "model/VMD/charge.vmd";
	vmdLoader_->LoadFile(strMotionPath.c_str());
	animDataMap_ = vmdLoader_->GetAnimationData();
	for (auto& anim : animDataMap_)
	{
		std::sort(anim.second.begin(), anim.second.end(), [](const Keyframe& a, const Keyframe& b) {return a.frameno < b.frameno; });
		for (auto& f : anim.second)
		{
			
			maxFrame = maxFrame < f.frameno ? f.frameno : maxFrame;
		}
	}
	maxFrame *= 2;

	CreateDepthBufferView();
	CreateVertexBuffer();
	ConstantBuffer();
	CreateConstantBufferView();
	CreateMaterialBuffer();
	InitRootSignature();
	CreatePipelineState();
	InitViewPort();
	InitScissorRect();
	CreateIndexArray();
	CreateBoneBuffer();

	//auto result = dev_->CreateCommittedResource();
	//assert(SUCCEEDED(result));
	return true;
}

bool Dx12Wrapper::Update()
{
	static float angle = 0.0f;
	static int frameno = 0;
	mat->world = XMMatrixRotationY(angle);
	angle += 0.01f;
	auto bbIndex = swapchain_->GetCurrentBackBufferIndex();
	D3D12_RESOURCE_BARRIER barrierDesc = {};
	ResourceBarrier(renderTargets_[bbIndex].Get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
	ClearRenderTarget();
	
	cmdList_->RSSetViewports(1, &viewPort_);
	cmdList_->SetPipelineState(pipeline_.Get());
	cmdList_->RSSetScissorRects(1, &scissorRect_);
	cmdList_->SetGraphicsRootSignature(rootSignature_.Get());
	cmdList_->SetDescriptorHeaps(1, basicDescHeap_.GetAddressOf());
	cmdList_->SetGraphicsRootDescriptorTable(0, basicDescHeap_->GetGPUDescriptorHandleForHeapStart());
	cmdList_->IASetVertexBuffers(0, 1, &vbView_);
	cmdList_->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	cmdList_->IASetIndexBuffer(&ibView_);

	cmdList_->SetDescriptorHeaps(1, boneHeap_.GetAddressOf());
	cmdList_->SetGraphicsRootDescriptorTable(2, boneHeap_->GetGPUDescriptorHandleForHeapStart());

	cmdList_->SetDescriptorHeaps(1, materialHeap_.GetAddressOf());

	auto material = pmdModel->GetMaterial();
	uint32_t indexOffset = 0;
	auto matHandle = materialHeap_->GetGPUDescriptorHandleForHeapStart();
	auto cbvsrvIncSize = 5 * dev_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	
	static auto lastTime = GetTickCount();	
	/*if (GetTickCount() - lastTime > maxFrame * 33.33333f)
	{
		lastTime = GetTickCount();
	}*/
	std::fill(boneMatrix_.begin(),boneMatrix_.end(),XMMatrixIdentity());
	AnimationUpdate(frameno / 2);
	std::copy(boneMatrix_.begin(), boneMatrix_.end(), mappedBonesMatrix_);

	int i = 0;
	for (auto& m : material)
	{
		if (i != 13 && i != 14)
		{
			cmdList_->SetGraphicsRootDescriptorTable(1, matHandle);
			auto indexNum = m.indicesNum;
			cmdList_->DrawIndexedInstanced(indexNum, 1, indexOffset, 0, 0);
			indexOffset += indexNum;
			matHandle.ptr += cbvsrvIncSize;
		}
		i++;
	}
	
	ResourceBarrier(renderTargets_[bbIndex].Get(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);

	cmdList_->Close();
	ID3D12CommandList* cmdLists[] = { cmdList_.Get() };
	cmdQue_->ExecuteCommandLists(1, cmdLists);
	WaitFence();

	swapchain_->Present(1, 0);

	cmdAllocator_->Reset();
	cmdList_->Reset(cmdAllocator_.Get(), pipeline_.Get());

	frameno = (frameno+1) % (maxFrame);
	return true;
}

bool Dx12Wrapper::ResourceBarrier(ComPtr<ID3D12Resource> resource,D3D12_RESOURCE_STATES before, D3D12_RESOURCE_STATES after)
{
	D3D12_RESOURCE_BARRIER barrierDesc = {};
	barrierDesc.Transition.pResource = resource.Get();
	barrierDesc.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	barrierDesc.Transition.StateBefore = before;
	barrierDesc.Transition.StateAfter = after;
	barrierDesc.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	barrierDesc.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;

	cmdList_->ResourceBarrier(1, &barrierDesc);

	return true;
}

void Dx12Wrapper::ClearRenderTarget()
{
	auto bbIndex = swapchain_->GetCurrentBackBufferIndex();
	int descriptorSize = dev_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	auto heapHandle = rtvHeap_->GetCPUDescriptorHandleForHeapStart();
	float clearColor[] = { 0.0f,0.0f,0.0f,1.0f };	// クリアカラー設定

	heapHandle.ptr += static_cast<ULONG_PTR>(bbIndex * descriptorSize);

	cmdList_->OMSetRenderTargets(1, &heapHandle, false, &depthHeap_->GetCPUDescriptorHandleForHeapStart()); // レンダーターゲット設定
	cmdList_->ClearDepthStencilView(depthHeap_->GetCPUDescriptorHandleForHeapStart(),D3D12_CLEAR_FLAG_DEPTH,1.0f,0,0,nullptr);
	cmdList_->ClearRenderTargetView(heapHandle, clearColor, 0, nullptr); // クリア
}

bool Dx12Wrapper::WaitFence()
{
	auto result = dev_->CreateFence(fenceValue_, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence_));
	assert(SUCCEEDED(result));

	fenceValue_++;
	cmdQue_->Signal(fence_.Get(), fenceValue_);

	// 処理がすべて終わるまで待つ
	while (1)
	{
		if (fence_->GetCompletedValue() == fenceValue_)
		{
			break;
		}
		else
		{
			auto event = CreateEvent(nullptr, false, false, nullptr);
			fence_->SetEventOnCompletion(fenceValue_, event);
			WaitForSingleObject(event, INFINITE);
			CloseHandle(event);
		}
	}

	return true;
}

void Dx12Wrapper::InitRootSignature()
{
	HRESULT result = S_OK;
	// ルートシグネチャ
	D3D12_ROOT_SIGNATURE_DESC rootSigDesc = {};
	CD3DX12_ROOT_PARAMETER rootParam[3] = {};
	CD3DX12_DESCRIPTOR_RANGE descRange[4] = {};
	std::array<CD3DX12_STATIC_SAMPLER_DESC, 2> samplerDesc = {};

	samplerDesc = SettingSampler();

	// 座標
	// 引数:rangeType,numDescriptors,baseShaderRagister
	descRange[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	// マテリアル
	descRange[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
	// テクスチャ
	descRange[2].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV,4,0);

	descRange[3].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV,1,2);

	// ルートパラメータ
	rootParam[0].InitAsDescriptorTable(1, &descRange[0]);
	rootParam[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;

	rootParam[1].InitAsDescriptorTable(2, &descRange[1]);
	rootParam[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParam[2].InitAsDescriptorTable(1, &descRange[3]);
	rootParam[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;

	rootSigDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
	rootSigDesc.pParameters = rootParam;
	rootSigDesc.NumParameters = 3;
	rootSigDesc.pStaticSamplers = &samplerDesc[0];
	rootSigDesc.NumStaticSamplers = 2;

	result = D3D12SerializeRootSignature(&rootSigDesc,
		D3D_ROOT_SIGNATURE_VERSION_1_0,
		&signatureBlob_,
		&errorBlob_);
	assert(SUCCEEDED(result));

	result = dev_->CreateRootSignature(0,
		signatureBlob_->GetBufferPointer(),
		signatureBlob_->GetBufferSize(),
		IID_PPV_ARGS(&rootSignature_));
	assert(SUCCEEDED(result));
}

void Dx12Wrapper::CreatePipelineState()
{
	HRESULT result = S_OK;
	D3D12_GRAPHICS_PIPELINE_STATE_DESC plsDesc = {};
	D3D12_INPUT_ELEMENT_DESC layout[] = {{"POSITION",0,DXGI_FORMAT_R32G32B32_FLOAT,0,0,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0},
										 {"NORMAL",0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0},
										 {"TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0},
										 {"BONENO",0,DXGI_FORMAT_R16G16_UINT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0},
										 {"WEIGHT",0,DXGI_FORMAT_R8_UINT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0} };

	plsDesc.InputLayout.NumElements = _countof(layout);
	plsDesc.InputLayout.pInputElementDescs = layout;
	plsDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

	// 頂点シェーダ
	result = D3DCompileFromFile(
		_T("Shader/VertexShader.hlsl"), // シェーダファイルパス
		nullptr, // defineマクロオブジェクト
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // includeオブジェクト
		"BasicVS",  // エントリポイント名
		"vs_5_1",	// シェーダバージョン
		D3DCOMPILE_DEBUG |
		D3DCOMPILE_SKIP_OPTIMIZATION,
		0,
		&vertexShader_,
		&errorBlob_);
	assert(SUCCEEDED(result));

	if (errorBlob_ != nullptr)
	{
		std::string errstr = "";
		auto errSize = errorBlob_->GetBufferSize();
		errstr.resize(errSize);
		std::copy_n(static_cast<char*>(errorBlob_->GetBufferPointer()), errorBlob_->GetBufferSize(), errstr.begin());
	}
	plsDesc.VS.BytecodeLength = vertexShader_->GetBufferSize();
	plsDesc.VS.pShaderBytecode = vertexShader_->GetBufferPointer();

	// ピクセルシェーダ
	result = D3DCompileFromFile(
		_T("Shader/PixelShader.hlsl"),
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"BasicPS",
		"ps_5_1",
		D3DCOMPILE_DEBUG |
		D3DCOMPILE_SKIP_OPTIMIZATION,
		0,
		&pixelShader_,
		&errorBlob_);
	assert(SUCCEEDED(result));

	if (errorBlob_ != nullptr)
	{
		std::string errstr = "";
		auto errSize = errorBlob_->GetBufferSize();
		errstr.resize(errSize);
		std::copy_n(static_cast<char*>(errorBlob_->GetBufferPointer()), errorBlob_->GetBufferSize(), errstr.begin());
	}
	plsDesc.PS.BytecodeLength = pixelShader_->GetBufferSize();
	plsDesc.PS.pShaderBytecode = pixelShader_->GetBufferPointer();

	// ラスタライザ設定
	plsDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;
	plsDesc.RasterizerState.FillMode = D3D12_FILL_MODE_SOLID;
	plsDesc.RasterizerState.DepthClipEnable = true;

	// 深度・ステンシル設定
	plsDesc.DepthStencilState.DepthEnable = true;
	plsDesc.DepthStencilState.StencilEnable = false;
	plsDesc.NodeMask = 0;
	plsDesc.SampleDesc.Count = 1;
	plsDesc.SampleDesc.Quality = 0;
	plsDesc.SampleMask = 0xffffffff;
	plsDesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	plsDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	plsDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
	
	// 出力設定
	plsDesc.BlendState.AlphaToCoverageEnable = false;
	plsDesc.BlendState.IndependentBlendEnable = false;
	plsDesc.BlendState.RenderTarget[0].BlendEnable = false;
	plsDesc.BlendState.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	plsDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	plsDesc.pRootSignature = rootSignature_.Get();
	plsDesc.NumRenderTargets = 1;
	
	result = dev_->CreateGraphicsPipelineState(&plsDesc, IID_PPV_ARGS(pipeline_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));
}

void Dx12Wrapper::CreateVertexBuffer()
{
	HRESULT result = S_OK;
	result = dev_->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(t_Vertex) * vertices_.size()),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&vertexBuffer_));
	assert(SUCCEEDED(result));

	t_Vertex* verMap = nullptr;
	result = vertexBuffer_->Map(0, nullptr, (void**)&verMap);
	assert(SUCCEEDED(result));
	std::copy(vertices_.begin(), vertices_.end(), verMap);
	vertexBuffer_->Unmap(0, nullptr);

	vbView_.BufferLocation = vertexBuffer_->GetGPUVirtualAddress();
	vbView_.SizeInBytes = sizeof(t_Vertex) * vertices_.size();
	vbView_.StrideInBytes = sizeof(vertices_[0]);
}

void Dx12Wrapper::InitViewPort()
{
	auto& app = Application::Instance();
	auto wsize = app.GetWindowSize();

	viewPort_.TopLeftX = 0;
	viewPort_.TopLeftY = 0;
	viewPort_.Width = wsize.w;
	viewPort_.Height = wsize.h;
	viewPort_.MaxDepth = 1.0f;
	viewPort_.MinDepth = 0.0f;
}

void Dx12Wrapper::InitScissorRect()
{
	auto& app = Application::Instance();
	auto wsize = app.GetWindowSize();

	scissorRect_.left = 0;
	scissorRect_.top = 0;
	scissorRect_.right = wsize.w;
	scissorRect_.bottom = wsize.h;
}

void Dx12Wrapper::CreateIndexArray()
{
	indices_ = pmdModel->GetIndices();
	HRESULT result = S_OK;

	result = dev_->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(indices_.size() * sizeof(indices_[0])),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(idxBuff_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	unsigned short *mappedIndex = nullptr;
	result = idxBuff_->Map(0, nullptr, (void**)&mappedIndex);
	assert(SUCCEEDED(result));
	std::copy(indices_.begin(), indices_.end(), mappedIndex);
	idxBuff_->Unmap(0, nullptr);

	ibView_.BufferLocation = idxBuff_->GetGPUVirtualAddress();	// バッファの場所
	ibView_.Format = DXGI_FORMAT_R16_UINT;		// フォーマット
	ibView_.SizeInBytes = indices_.size() * sizeof(indices_[0]);
}

void Dx12Wrapper::CreateConstantBufferView()
{
	D3D12_DESCRIPTOR_HEAP_DESC descriptorHeapDesc = {};
	descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	descriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	descriptorHeapDesc.NumDescriptors = 1;
	descriptorHeapDesc.NodeMask = 0;
	auto result = dev_->CreateDescriptorHeap(&descriptorHeapDesc, IID_PPV_ARGS(basicDescHeap_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc = {};
	cbvDesc.BufferLocation = cbvBuffer_->GetGPUVirtualAddress();
	cbvDesc.SizeInBytes = cbvBuffer_->GetDesc().Width;
	dev_->CreateConstantBufferView(&cbvDesc, basicDescHeap_->GetCPUDescriptorHandleForHeapStart());
}

void Dx12Wrapper::ConstantBuffer()
{
	HRESULT result = S_OK;

	D3D12_RESOURCE_DESC resourceDesc = {};
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	resourceDesc.Width = AlignedValue(sizeof(SceneData), D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT);
	resourceDesc.Height = 1;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.MipLevels = 1;
	resourceDesc.Format = DXGI_FORMAT_UNKNOWN;
	resourceDesc.SampleDesc.Count = 1;
	resourceDesc.Flags = D3D12_RESOURCE_FLAG_NONE;
	resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;

	result = dev_->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAGS::D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(cbvBuffer_.ReleaseAndGetAddressOf())
	);
	assert(SUCCEEDED(result));

	Up = XMFLOAT3(0, 1, 0);
	Eye = XMFLOAT3(0.0f,18.0f, -25.0f);
	Target = XMFLOAT3(0, 14, 0);
	
	cbvBuffer_->Map(0, nullptr, (void**)&mat);

	mat->world = XMMatrixRotationY(XM_PIDIV4);
	mat->view  = XMMatrixLookAtLH(XMLoadFloat3(&Eye), XMLoadFloat3(&Target), XMLoadFloat3(&Up));
	mat->prj   = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), 4.0f / 3.0f, 0.1f, 300.0f);
	mat->eye   = Eye;
}

void Dx12Wrapper::CreateDepthBufferView()
{
	HRESULT result = S_OK;
	auto& app = Application::Instance();
	auto wsize = app.GetWindowSize();

	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = DXGI_FORMAT_D32_FLOAT;
	resDesc.Width = wsize.w;
	resDesc.Height = wsize.h;
	resDesc.DepthOrArraySize = 1;
	resDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
	resDesc.MipLevels = 1;
	resDesc.SampleDesc.Count = 1;
	resDesc.SampleDesc.Quality = 0;
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

	D3D12_CLEAR_VALUE clearValue = {};
	clearValue.Format = DXGI_FORMAT_D32_FLOAT;
	clearValue.DepthStencil.Depth = 1.0f;

	result = dev_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&clearValue,
		IID_PPV_ARGS(depthBuffer_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	D3D12_DESCRIPTOR_HEAP_DESC devHeapDesc = {};
	ID3D12DescriptorHeap* dsvHeap = nullptr;
	devHeapDesc.NumDescriptors = 1;
	devHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	result = dev_->CreateDescriptorHeap(&devHeapDesc, IID_PPV_ARGS(depthHeap_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Texture2D.MipSlice = 0;
	dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;

	dev_->CreateDepthStencilView(depthBuffer_.Get(), &dsvDesc, depthHeap_->GetCPUDescriptorHandleForHeapStart());
}

void Dx12Wrapper::CreateMaterialBuffer()
{
	HRESULT result = S_OK;

	D3D12_HEAP_PROPERTIES heapProp = {};
	heapProp.Type = D3D12_HEAP_TYPE_UPLOAD;
	auto& materials = pmdModel->GetMaterial();
	auto strideBytes = AlignedValue(sizeof(BasicMaterial), D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT);

	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = DXGI_FORMAT_UNKNOWN;
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;
	resDesc.Width = materials.size() * strideBytes;
	resDesc.Height = 1;
	resDesc.DepthOrArraySize = 1;
	resDesc.MipLevels = 1;
	resDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	resDesc.SampleDesc.Count = 1;
	resDesc.SampleDesc.Quality = 0;

	result = dev_->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(materialBuffer_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	D3D12_DESCRIPTOR_HEAP_DESC materialDescHeapDesc;
	materialDescHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	materialDescHeapDesc.NodeMask = 0;
	materialDescHeapDesc.NumDescriptors = materials.size() * 5;
	materialDescHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	result = dev_->CreateDescriptorHeap(&materialDescHeapDesc, IID_PPV_ARGS(materialHeap_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	auto gAdress = materialBuffer_->GetGPUVirtualAddress();
	auto heapSize = dev_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	auto heapAdress = materialHeap_->GetCPUDescriptorHandleForHeapStart();
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc = {};
	cbvDesc.SizeInBytes = strideBytes;

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;

	uint8_t* mappedMaterial = nullptr;
	result = materialBuffer_->Map(0,nullptr,(void**)&mappedMaterial);
	assert(SUCCEEDED(result));

	// グラデーションテクスチャ
	auto gradTex = CreateGrayGradationTexture();

	for (int i = 0; i < materials.size(); i++)
	{
		((BasicMaterial*)mappedMaterial)->diffuse = materials[i].diffuse;
		((BasicMaterial*)mappedMaterial)->specular = materials[i].specular;
		((BasicMaterial*)mappedMaterial)->ambient =  materials[i].ambient;
		((BasicMaterial*)mappedMaterial)->alpha =  materials[i].alpha;
		((BasicMaterial*)mappedMaterial)->specularity =  materials[i].specularity;

		cbvDesc.BufferLocation = gAdress;
		dev_->CreateConstantBufferView(&cbvDesc,heapAdress);
		mappedMaterial += strideBytes;
		gAdress += strideBytes;
		heapAdress.ptr += heapSize;

		srvDesc.Format = texResources_[i]["bmp"]->GetDesc().Format;
		dev_->CreateShaderResourceView(texResources_[i]["bmp"].Get(), &srvDesc, heapAdress);
		heapAdress.ptr += heapSize;

		srvDesc.Format = texResources_[i]["sph"]->GetDesc().Format;
		dev_->CreateShaderResourceView(texResources_[i]["sph"].Get(), &srvDesc, heapAdress);
		heapAdress.ptr += heapSize;

		srvDesc.Format = texResources_[i]["spa"]->GetDesc().Format;
		dev_->CreateShaderResourceView(texResources_[i]["spa"].Get(), &srvDesc, heapAdress);
		heapAdress.ptr += heapSize;

		if (toonResources_[i] == nullptr)
		{
			srvDesc.Format = gradTex->GetDesc().Format;
			dev_->CreateShaderResourceView(gradTex.Get(), &srvDesc, heapAdress);
		}
		else
		{
			srvDesc.Format = toonResources_[i]->GetDesc().Format;
			dev_->CreateShaderResourceView(toonResources_[i].Get(), &srvDesc, heapAdress);
		}
		heapAdress.ptr += heapSize;
	}

	materialBuffer_->Unmap(0, nullptr);
}

Microsoft::WRL::ComPtr<ID3D12Resource> Dx12Wrapper::LoadTextureFromFile(std::string& texPath)
{
	TexMetadata metadata = {};
	ScratchImage scratchImg = {};

	auto it = resourceTbl_.find(texPath);

	HRESULT result = S_OK;

	if (it != resourceTbl_.end())
	{
		return resourceTbl_[texPath];
	}

	if (GetExtension(texPath) == "tga")
	{
		result = LoadFromTGAFile(GetWideStringFromString(texPath).c_str(),
			&metadata,
			scratchImg);
		assert(SUCCEEDED(result));
	}
	else
	{
		result = LoadFromWICFile(GetWideStringFromString(texPath).c_str(),
			WIC_FLAGS_NONE,
			&metadata,
			scratchImg);
		assert(SUCCEEDED(result));
	}

	auto img = scratchImg.GetImage(0, 0, 0);

	// WriteToSubresourceで転送する用のヒープ設定
	D3D12_HEAP_PROPERTIES texHeapProp = {};
	texHeapProp.Type = D3D12_HEAP_TYPE_CUSTOM;
	texHeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
	texHeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_L0;
	texHeapProp.CreationNodeMask = 0;
	texHeapProp.VisibleNodeMask = 0;

	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = metadata.format;
	resDesc.Width = metadata.width;
	resDesc.Height = metadata.height;
	resDesc.DepthOrArraySize = metadata.arraySize;
	resDesc.SampleDesc.Count = 1;
	resDesc.SampleDesc.Quality = 0;
	resDesc.MipLevels = metadata.mipLevels;
	resDesc.Dimension = static_cast<D3D12_RESOURCE_DIMENSION>(metadata.dimension);
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	ID3D12Resource* texbuff = nullptr;
	result = dev_->CreateCommittedResource(
		&texHeapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&texbuff));
	assert(SUCCEEDED(result));

	result = texbuff->WriteToSubresource(
		0,
		nullptr,
		img->pixels,
		img->rowPitch,
		img->slicePitch);
	assert(SUCCEEDED(result));

	resourceTbl_[texPath] = texbuff;

	return texbuff;
}

ComPtr<ID3D12Resource> Dx12Wrapper::CreateAnotherTexture(bool whiteTexture)
{
	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	resDesc.Width = 4;
	resDesc.Height = 4;
	resDesc.DepthOrArraySize = 1;
	resDesc.SampleDesc.Count = 1;
	resDesc.SampleDesc.Quality = 0;
	resDesc.MipLevels = 1;
	resDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	D3D12_HEAP_PROPERTIES heapProp = {};
	heapProp.Type = D3D12_HEAP_TYPE_CUSTOM;
	heapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
	heapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_L0;
	heapProp.CreationNodeMask = 0;
	heapProp.VisibleNodeMask = 0;

	ID3D12Resource* texbuff = nullptr;
	auto result = dev_->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&texbuff));
	assert(SUCCEEDED(result));

	std::vector<uint8_t> data(4 * 4 * 4);
	if (whiteTexture)
	{
		std::fill(data.begin(), data.end(), 0xff);
		result = texbuff->WriteToSubresource(
			0,
			nullptr,
			data.data(),
			4 * 4,
			data.size());
	}
	else
	{
		std::fill(data.begin(), data.end(), 0);
		result = texbuff->WriteToSubresource(
			0,
			nullptr,
			data.data(),
			4 * 4,
			data.size());
	}
	assert(SUCCEEDED(result));

	return texbuff;
}

bool Dx12Wrapper::CreateTexture(std::string& texPath, Microsoft::WRL::ComPtr<ID3D12Resource>& toonRes)
{
	TexMetadata metadata = {};
	ScratchImage scratchImg = {};

	auto result = LoadFromWICFile(GetWideStringFromString(texPath).c_str(),WIC_FLAGS_NONE,&metadata,scratchImg);
	if (result != S_OK)
	{
		return false;
	}

	// WriteToSubresourceで転送する用のヒープ設定
	D3D12_HEAP_PROPERTIES texHeapProp = {};
	texHeapProp.Type = D3D12_HEAP_TYPE_CUSTOM;
	texHeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
	texHeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_L0;
	texHeapProp.CreationNodeMask = 0;
	texHeapProp.VisibleNodeMask = 0;

	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = metadata.format;
	resDesc.Width = metadata.width;
	resDesc.Height = metadata.height;
	resDesc.DepthOrArraySize = metadata.arraySize;
	resDesc.SampleDesc.Count = 1;
	resDesc.SampleDesc.Quality = 0;
	resDesc.MipLevels = metadata.mipLevels;
	resDesc.Dimension = static_cast<D3D12_RESOURCE_DIMENSION>(metadata.dimension);
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	ID3D12Resource* texbuff = nullptr;
	result = dev_->CreateCommittedResource(
		&texHeapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&texbuff));
	if (result != S_OK)
	{
		return false;
	}

	auto img = scratchImg.GetImage(0, 0, 0);
	result = texbuff->WriteToSubresource(
		0,
		nullptr,
		img->pixels,
		img->rowPitch,
		img->slicePitch);
	if (result != S_OK)
	{
		return false;
	}

	toonRes = texbuff;

	return true;
}

ComPtr<ID3D12Resource> Dx12Wrapper::CreateGrayGradationTexture()
{
	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	resDesc.Width = 4;
	resDesc.Height = 256;
	resDesc.DepthOrArraySize = 1;
	resDesc.SampleDesc.Count = 1;
	resDesc.SampleDesc.Quality = 0;
	resDesc.MipLevels = 1;
	resDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	D3D12_HEAP_PROPERTIES heapProp = {};
	heapProp.Type = D3D12_HEAP_TYPE_CUSTOM;
	heapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
	heapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_L0;
	heapProp.CreationNodeMask = 0;
	heapProp.VisibleNodeMask = 0;

	ID3D12Resource* gradbuff = nullptr;
	auto result = dev_->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&gradbuff));
	assert(SUCCEEDED(result));

	std::vector<unsigned int> data(4 * 256);
	auto it = data.begin();
	unsigned int c = 0xff;

	for (; it != data.end(); it += 4)
	{
		auto col = (c << 0xff) | (c << 16) | (c << 8) | c;
		std::fill(it, it + 4, col);
		--c;
	}

	result = gradbuff->WriteToSubresource(
		0,
		nullptr,
		data.data(),
		4 * sizeof(unsigned int),
		sizeof(unsigned int) * data.size());

	return gradbuff;
}

std::wstring Dx12Wrapper::GetWideStringFromString(const std::string& str)
{
	//呼び出し1回目(文字列数を得る)
	auto num1 = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED | MB_ERR_INVALID_CHARS,str.c_str(), -1, nullptr, 0);
	
	std::wstring wstr;//stringのwchar_t版
	wstr.resize(num1);//得られた文字列数でリサイズ
	
	//呼び出し2回目(確保済みのwstrに変換文字列をコピー)
	auto num2 = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED | MB_ERR_INVALID_CHARS,str.c_str(), -1, &wstr[0], num1);
	assert(num1 == num2);

	return wstr;
}

std::string Dx12Wrapper::GetTexturePathFromModelAndTexPath(const std::string& modelPath, const char* texpath)
{
	int pathIndex1 = modelPath.rfind('/');
	int pathIndex2 = modelPath.rfind('\\');
	auto pathIndex = max(pathIndex1, pathIndex2);
	auto folderPath = modelPath.substr(0, pathIndex + 1);

	return folderPath + texpath;
}

std::string Dx12Wrapper::GetExtension(const std::string& path)
{
	int index = path.rfind('.');

	return path.substr(index + 1,path.length() - index - 1);
}

pair<string, string> Dx12Wrapper::SplitFileName(const std::string& path, const char splitter)
{
	int index = path.find(splitter);
	pair <string, string> ret;
	ret.first = path.substr(0, index);
	ret.second = path.substr(index + 1, path.length() - index - 1);

	return ret;
}

unsigned int Dx12Wrapper::AlignedValue(unsigned int value, unsigned int aligne)
{
	return value + (aligne - value % aligne);
}

std::array<CD3DX12_STATIC_SAMPLER_DESC,2> Dx12Wrapper::SettingSampler()
{
	std::array<CD3DX12_STATIC_SAMPLER_DESC, 2> samplerDesc = {};
	samplerDesc[0].Init(0, D3D12_FILTER_MIN_MAG_MIP_POINT);
	samplerDesc[0].BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK; // エッジの色(黒透明)
	samplerDesc[0].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;	// 常に通る
	samplerDesc[1].Init(1, D3D12_FILTER_ANISOTROPIC, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, 
						D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP);
	return samplerDesc;
}

void Dx12Wrapper::CreateBoneBuffer()
{
	HRESULT result = S_OK;

	auto size = AlignedValue(sizeof(XMMATRIX), D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT);

	CD3DX12_HEAP_PROPERTIES heapProp(D3D12_HEAP_TYPE_UPLOAD);
	/*heapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	heapProp.CreationNodeMask = 1;
	heapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	heapProp.Type = D3D12_HEAP_TYPE_UPLOAD;
	heapProp.VisibleNodeMask = 1;*/

	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = DXGI_FORMAT_UNKNOWN;
	resDesc.DepthOrArraySize = 1;
	resDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;
	resDesc.Height = 1;
	resDesc.Width = boneMatrix_.size() * size;
	resDesc.SampleDesc.Count = 1;
	resDesc.MipLevels = 1;
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;

	result = dev_->CreateCommittedResource(&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(boneBuffer_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	D3D12_DESCRIPTOR_HEAP_DESC descHeapDesc = {};
	descHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	descHeapDesc.NodeMask = 0;
	descHeapDesc.NumDescriptors = 1;
	descHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	result = dev_->CreateDescriptorHeap(&descHeapDesc, IID_PPV_ARGS(boneHeap_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	D3D12_CONSTANT_BUFFER_VIEW_DESC desc = {};
	desc.BufferLocation = boneBuffer_->GetGPUVirtualAddress();
	desc.SizeInBytes = size * boneMatrix_.size();
	auto handle = boneHeap_->GetCPUDescriptorHandleForHeapStart();
	dev_->CreateConstantBufferView(&desc, handle);

	result = boneBuffer_->Map(0,nullptr,(void**)&mappedBonesMatrix_);
	assert(SUCCEEDED(result));
}

void Dx12Wrapper::AnimationUpdate(int frameno)
{
	for (auto& boneanim : animDataMap_)
	{
		auto& keyframes = boneanim.second;

		auto frameIt = std::find_if(keyframes.rbegin(), keyframes.rend(),
			[frameno](const Keyframe& k) 
			{
				return k.frameno <= frameno; 
			});

		if (frameIt == keyframes.rend())
		{
			continue;
		}

		auto frameIt2 = frameIt.base();
		if (frameIt2 == keyframes.end())
		{
			RotateBone(boneanim.first.c_str(), frameIt->quaternion,XMFLOAT4(),0);
			continue;
		}

		auto framePer = (static_cast<float>(frameno) - frameIt->frameno) / (frameIt2->frameno - frameIt->frameno);

		RotateBone(boneanim.first.c_str(), frameIt->quaternion, frameIt2->quaternion,framePer);
	}

	XMMATRIX rootmat = XMMatrixIdentity();
	RecursiveMatrixMultiply(boneMap_["センター"], rootmat);
}

void Dx12Wrapper::RotateBone(const char* bonename, const DirectX::XMFLOAT4& q, const DirectX::XMFLOAT4& q2,const float framePer)
{
	auto& bonenode = boneMap_[bonename];
	auto vec = XMLoadFloat3(&bonenode.startPos);
	auto quaternion = XMLoadFloat4(&q);
	auto quaternion2 = XMLoadFloat4(&q2);
	auto newQuaternion = (1 - framePer) * quaternion + quaternion2 * framePer;
	boneMatrix_[bonenode.boneIdx] = XMMatrixTranslationFromVector(XMVectorScale(vec, -1))
									* XMMatrixRotationQuaternion(XMQuaternionSlerp(quaternion, quaternion2, framePer))
									* XMMatrixTranslationFromVector(vec);
}

void Dx12Wrapper::RecursiveMatrixMultiply(BoneNode& node, XMMATRIX& inMat)
{
	boneMatrix_[node.boneIdx] *= inMat;
	for (auto& cnode : node.children)
	{
		RecursiveMatrixMultiply(*cnode,boneMatrix_[node.boneIdx]);
	}
}

float Dx12Wrapper::GetYFromXOnBezierHalfSolve(float x, const DirectX::XMFLOAT2& a, const DirectX::XMFLOAT2& b, uint8_t n)
{
	if (a.x == a.y && b.x == b.y)
	{
		return x;
	}
	
	float t = x;
	const float k1 = 1 + 3 * a.x - 3 * b.x;	// t^3の係数
	const float k2 = 3 * b.x - 6 * a.x;		// t^2の係数
	const float k3 = 3 * a.x;				// tの係数

	// 誤差の範囲内かどうかに使用する定数
	constexpr float epsilon = 0.0005f;

	return 0.0f;
}

void Dx12Wrapper::Terminate()
{
}