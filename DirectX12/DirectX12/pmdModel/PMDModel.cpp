#include "PMDModel.h"
#include <stdio.h>


PMDModel::PMDModel()
{
}

bool PMDModel::LoadFile(const char* filePath)
{
	char magic[3] = {};
	uint32_t vertexCount;

	uint32_t indexNum = 0;
	uint32_t materialNum = 0;
	
	FILE* file;
	fopen_s(&file, filePath, "rb");

	std::vector<t_Vertex> vert;
	PMDHeader pmdheader;
	fread(&magic, sizeof(magic), 1, file);
	fread(&pmdheader, sizeof(pmdheader), 1, file);
	fread(&vertexCount, sizeof(vertexCount), 1, file);

	pmdvertices_.resize(vertexCount);
	vert.resize(vertexCount);
	fread(vert.data(), sizeof(t_Vertex) * vert.size(), 1, file);
	for (int vertexNum = 0; vertexNum < pmdvertices_.size(); vertexNum++)
	{
		pmdvertices_[vertexNum].pos = vert[vertexNum].pos;
		pmdvertices_[vertexNum].normal_vec = vert[vertexNum].normal_vec;
		pmdvertices_[vertexNum].uv = vert[vertexNum].uv;
		pmdvertices_[vertexNum].bone_num[0] = vert[vertexNum].bone_num[0];
		pmdvertices_[vertexNum].bone_num[1] = vert[vertexNum].bone_num[1];
		pmdvertices_[vertexNum].bone_weight = vert[vertexNum].bone_weight;
		pmdvertices_[vertexNum].edge_flag = vert[vertexNum].edge_flag;
	}

	fread(&indexNum, sizeof(indexNum), 1, file);
	pmdIndices_.resize(indexNum);
	fread(pmdIndices_.data(), sizeof(uint16_t), pmdIndices_.size(), file);
	fread(&materialNum, sizeof(materialNum), 1, file);
	pmdMaterials_.resize(materialNum);
	fread(pmdMaterials_.data(), sizeof(Material), pmdMaterials_.size(), file);
	
	// ボーン読み込み
	uint16_t boneNum = 0;
	fread_s(&boneNum, sizeof(boneNum), sizeof(boneNum), 1,file);
	bone_.resize(boneNum);
	fread(bone_.data(), sizeof(Bone),bone_.size(), file);

	bones_.resize(boneNum);
	for (int i = 0; i < boneNum; i++)
	{
		bones_[i].name = bone_[i].bone_name;
		bones_[i].pos = bone_[i].bone_head_pos;
	}

	for (int i = 0; i < boneNum; i++)
	{
		if (bone_[i].parent_bone_index == 0xffff)
		{
			auto pno = bone_[i].parent_bone_index;
			bones_[i].children.push_back(i);
			bones_[i].childrenName.push_back(bone_[i].bone_name);
		}
	}

	// IK
	unsigned short ikNum = 0;
	fread_s(&ikNum, sizeof(ikNum), sizeof(ikNum), 1, file);

	for (int i = 0; i < ikNum; i++)
	{
		fseek(file,4,SEEK_CUR);
		BYTE chainNum;
		fread_s(&chainNum, sizeof(chainNum), sizeof(chainNum), 1, file);
		fseek(file,6,SEEK_CUR);
		fseek(file,sizeof(WORD) * chainNum,SEEK_CUR);
	}

	// 表情
	WORD skinNum = 0;
	fread_s(&skinNum, sizeof(skinNum), sizeof(skinNum), 1, file);

	for (int i = 0; i < skinNum; i++)
	{
		fseek(file, 20, SEEK_CUR);
		DWORD skinVertNum = 0;
		fread_s(&skinVertNum, sizeof(skinVertNum), sizeof(skinVertNum), 1, file);
		fseek(file, 1, SEEK_CUR);
		fseek(file, skinVertNum * 16, SEEK_CUR);
	}

	BYTE skinDispNum = 0;
	fread_s(&skinDispNum, sizeof(skinDispNum), sizeof(skinDispNum), 1, file);
	fseek(file, skinDispNum * 2, SEEK_CUR);

	BYTE boneDisplayNameNum = 0;
	fread_s(&boneDisplayNameNum, sizeof(boneDisplayNameNum), sizeof(boneDisplayNameNum), 1, file);
	fseek(file, boneDisplayNameNum * 50,SEEK_CUR);

	DWORD boneDisplayNum = 0;
	fread_s(&boneDisplayNum, sizeof(boneDisplayNum), sizeof(boneDisplayNum), 1, file);
	fseek(file, boneDisplayNum * 3, SEEK_CUR);

	BYTE isEng = 0;
	fread_s(&isEng, sizeof(isEng), sizeof(isEng), 1, file);
	if (isEng)
	{
		fseek(file, 276, SEEK_CUR);
		fseek(file, boneNum * 20, SEEK_CUR);
		fseek(file, (skinNum - 1) * 20, SEEK_CUR);
		fseek(file, boneDisplayNameNum * 50, SEEK_CUR);
	}

	std::vector<std::string> toonTable;
	toonTable.reserve(10);
	for (int i = 0; i < 10; i++)
	{
		char cpath[100] = {};
		fread(&cpath, sizeof(cpath), 1, file);
		toonTable.push_back(cpath);
	}

	for (auto& m : pmdMaterials_)
	{
		if (m.toon_index == 0xff)
		{
			toonPaths_.push_back("");
		}
		else
		{
			toonPaths_.push_back(toonTable[m.toon_index]);
		}
	}

	fclose(file); 

	return false;
}

const std::vector<t_Vertex>& PMDModel::GetVertexData()
{
	return pmdvertices_;
}

const std::vector<uint16_t>& PMDModel::GetIndices()
{
	return pmdIndices_;
}

const std::vector<Material>& PMDModel::GetMaterial()
{
	return pmdMaterials_;
}

const std::vector<std::string>& PMDModel::GetToonPath()
{
	return toonPaths_;
}

const std::vector<Bone>& PMDModel::GetBone()
{
	return bone_;
}
