#include "common.hlsli"

Texture2D<float4> tex : register(t0);
Texture2D<float4> sph : register(t1);
Texture2D<float4> spa : register(t2);
Texture2D<float4> toon: register(t3);
SamplerState smp : register(s0);
SamplerState smpToon : register(s1);


cbuffer Material:register(b1)
{
	float4 diffuse;
	float4 specular;
	float4 ambient;
}


float4 BasicPS(Out o) : SV_TARGET
{
	float3 light = normalize(float3(1,-1,1));	// 光ベクトル

	// diffuse計算
	float diffuseB = saturate( dot(-light, o.normal) );

	// 光の反射ベクトル
	float3 refLight = normalize(reflect(light,o.normal.xyz));
	float specularB = pow(saturate(dot(refLight, -o.ray)), specular.a);

	// sphereMap用UV
	float2 sphereMapUV = o.vnormal.xy;
	sphereMapUV = (sphereMapUV + float2(1, -1)) * float2(0.5, -0.5);

	// テクスチャカラー
	float4 texColor = tex.Sample(smp, o.uv);
	//return texColor;
	
	// toon
	float4 toonDif = toon.Sample(smpToon,float2(0,1.0 - diffuseB));

	/*return float4(tex.Sample(smp,o.uv).rgb - fmod(tex.Sample(smp,o.uv).rgb,0.25f),1);*/

	return saturate(toonDif * diffuse * texColor * sph.Sample(smp, sphereMapUV)
					+ saturate(spa.Sample(smp,sphereMapUV) * texColor + float4(specularB * specular.rgb,1)
					+ float4(texColor.rgb * ambient * 0.1,1)));
}